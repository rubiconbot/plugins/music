/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;

public class CommandQueueRepeat extends SameChannelCommand {

    public CommandQueueRepeat() {
        super(new String[] {"queuerepeat", "queueloop", "qrepeat", "qloop"}, "", "Plays the current queue on repeat");
    }

    @Override
    Result run(CommandEvent event, String[] args) {
        GuildMusicPlayer guildMusicPlayer = getGuildMusicPlayer(event.getGuild());
        if(!guildMusicPlayer.isPlaying())
            return send(error("Not playing!", "Rubicon does not play any song at the moment."));
        if(guildMusicPlayer.isRepeating())
            return send(error("Already queue repeating!", "Rubicon does already loop the current song"));
        if(!guildMusicPlayer.isQueueRepeating()){
            guildMusicPlayer.setQueueRepeating(true);
            return send(success("Queuerepeat enabled", "Rubicon does now repeat the queue"));
        } else {
            guildMusicPlayer.setQueueRepeating(false);
            guildMusicPlayer.getQueue().clear();
            return send(success("Queuerepeat disabled", "Rubicon does no longer repeat the queue"));
        }    }
}
