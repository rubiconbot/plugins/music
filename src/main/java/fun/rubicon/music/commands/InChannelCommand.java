/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import fun.rubicon.music.MusicPlugin;
import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import net.dv8tion.jda.core.entities.Guild;

public abstract class InChannelCommand extends Command {

    InChannelCommand(String[] invocations,String usage, String description) {
        super(invocations, CommandCategory.MUSIC, usage, description);
    }


    @Override
    public Result execute(CommandEvent event, String[] args) {
        if(!event.getMember().getVoiceState().inVoiceChannel())
            return send(error("Not in a channel!", "You have to be in a voice channel in order to run this command."));
        return run(event, args);
    }

    abstract Result run(CommandEvent event, String[] args);

    GuildMusicPlayer getGuildMusicPlayer(Guild guild){
        return MusicPlugin.getInstance().getGuildMusicPlayerManager().getAndCreatePlayer(guild);
    }


}
