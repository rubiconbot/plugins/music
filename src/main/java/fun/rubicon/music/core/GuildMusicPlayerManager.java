/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.core;


import lombok.Getter;
import net.dv8tion.jda.core.entities.Guild;

import java.util.HashMap;

public class GuildMusicPlayerManager {

    @Getter
    private HashMap<Long, GuildMusicPlayer> playerStorage = new HashMap<>();

    public GuildMusicPlayer getAndCreatePlayer(Guild guild) {
        GuildMusicPlayer player;
        if (!playerStorage.containsKey(guild.getIdLong())) {
            player = new GuildMusicPlayer(guild);
            playerStorage.put(guild.getIdLong(), player);
        }
        else {
            player = playerStorage.get(guild.getIdLong());
        }
        return player;
    }

}
