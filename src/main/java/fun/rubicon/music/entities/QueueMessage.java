/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.entities;

import fun.rubicon.music.MusicPlugin;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class QueueMessage extends ListenerAdapter {

    private static HashMap<Long, QueueMessage> queueMessageStorage = new HashMap<>();

    private int sideNumbAll;
    private int currentSideNumb;
    private List<String> tracks;
    private User author;

    public QueueMessage(Integer sideNumbAll, Message message, List<String> tracks, User author) {
        this.sideNumbAll = sideNumbAll;
        this.currentSideNumb = 1;
        this.tracks = tracks;
        this.author = author;
        queueMessageStorage.put(message.getIdLong(), this);
    }

    public static void handleReaction(GuildMessageReactionAddEvent event) {
        if (event.getUser().isBot()) return;
        if (queueMessageStorage.containsKey(Long.parseLong(event.getMessageId()))) {
            event.getReaction().removeReaction(event.getUser()).queue();
            QueueMessage queueMessage = queueMessageStorage.get(event.getMessageIdLong());
            if (!queueMessage.author.equals(event.getUser())) return;
            Message message = event.getChannel().getMessageById(event.getMessageIdLong()).complete();
            String reaction = event.getReactionEmote().getName();
            if (reaction.equals("⬅")) {
                queueMessage.currentSideNumb--;
            } else if (reaction.equals("➡")) {
                queueMessage.currentSideNumb++;
            }
            List<String> tracks = queueMessage.tracks.subList((queueMessage.currentSideNumb - 1) * 20, (queueMessage.currentSideNumb - 1) * 20 + 20);
            message.getReactions().forEach(r -> r.removeReaction().queue());
            String formattedQueue = tracks.stream().collect(Collectors.joining("\n"));
            message.editMessage(new EmbedBuilder().setDescription("**CURRENT QUEUE:**\n" +
                    "*[" + MusicPlugin.getInstance().getGuildMusicPlayerManager().getPlayerStorage().get(event.getGuild().getIdLong()).getTrackList().size() + " Tracks | Side " + queueMessage.currentSideNumb + " / " + queueMessage.sideNumbAll + "]* \n" + formattedQueue).build()).queue();
            queueMessageStorage.replace(event.getMessageIdLong(), queueMessage);
            if (queueMessage.currentSideNumb > 1) {
                message.addReaction("⬅").queue();
            }

            if (queueMessage.currentSideNumb < queueMessage.sideNumbAll) {
                message.addReaction("➡").queue();
            }
        }
        //Stop Thread because we don't need him anymore
        Thread.currentThread().interrupt();
    }

    public static void handleMessageDeletion(GuildMessageDeleteEvent event) {
        if (queueMessageStorage.containsKey(Long.parseLong(event.getMessageId()))) {
            queueMessageStorage.remove(event.getGuild().getIdLong());
        }
    }
}
