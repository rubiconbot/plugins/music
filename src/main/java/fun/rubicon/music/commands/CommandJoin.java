/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import net.dv8tion.jda.core.Permission;

public class CommandJoin extends InChannelCommand {

    public CommandJoin() {
        super(new String[]{"join"}, "", "Joins your channel");
    }

    @Override
    Result run(CommandEvent event, String[] args) {
        GuildMusicPlayer musicPlayer = getGuildMusicPlayer(event.getGuild());
        if (!event.getGuild().getSelfMember().hasPermission(event.getMember().getVoiceState().getChannel(), Permission.VOICE_CONNECT))
            return send(error("No permissions!", "Rubicon is not permitted to join your channel."));
        musicPlayer.connect(event.getMember().getVoiceState().getChannel());
        return send(success("Joined!", "Successfully joined your channel."));
    }
}
