/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.entities;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.music.util.EmbedUtil;
import fun.rubicon.plugin.util.Colors;
import fun.rubicon.plugin.util.SafeMessage;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static net.dv8tion.jda.core.utils.Helpers.isNumeric;

public class MusicSearchResult {

    public static List<MusicSearchResult> musicChoose = new ArrayList<>();

    @Getter
    private final User user;
    private final List<AudioTrack> trackList;
    @Getter
    private final GuildMusicPlayer guildMusicPlayer;
    @Getter @Setter
    private Message chooseMessage;

    private final String[] emotes = {
            ":one:",
            ":two:",
            ":three:",
            ":four:",
            ":five:"
    };

    public MusicSearchResult(Member author, GuildMusicPlayer guildMusicPlayer) {
        this.guildMusicPlayer = guildMusicPlayer;
        this.user = author.getUser();
        trackList = new ArrayList<>();
    }

    public void addTrack(AudioTrack track) throws Exception {
        if (trackList.size() >= 5)
            throw new Exception("No support for more than 5 links.");
        trackList.add(track);
    }

    public EmbedBuilder generateEmbed() {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(Colors.BLUE);
        int i = 0;
        StringBuilder description = new StringBuilder();
        for (AudioTrack track : trackList) {
            description.append(emotes[i]).append("  [").append(track.getInfo().title).append("](").append(track.getInfo().uri).append(")\n\n");
            i++;
        }
        builder.setFooter("Select song by sending index. This will be aborted automatically after 15 seconds", null);
        builder.setDescription(description.toString());
        return builder;
    }

    public static void handleTrackChoose(GuildMessageReceivedEvent event) {
        List<MusicSearchResult> storage = musicChoose.stream().filter(musicSearchResult -> musicSearchResult.getUser() == event.getAuthor()).collect(Collectors.toList());
        if (storage.isEmpty()) {
            return;
        }
        MusicSearchResult result = storage.get(0);
        String response = event.getMessage().getContentDisplay();
        if (!isNumeric(response)) {
            return;
        }
        int ans = Integer.parseInt(response);
        if (ans < 1 || ans > 5) {
            return;
        }
        ans--;
        AudioTrack track = result.trackList.get(ans);
        TrackDataHolder trackData = new TrackDataHolder(track);
        result.getGuildMusicPlayer().queueTrack(track);

        EmbedBuilder embedBuilder = EmbedUtil.formatEmbed(trackData, result.user, result.guildMusicPlayer.getQueueSize());
        SafeMessage.sendMessage(event.getChannel(), embedBuilder.build());
        result.getChooseMessage().delete().queue();
        musicChoose.remove(result);
        if (event.getGuild().getSelfMember().hasPermission(event.getChannel(), Permission.MESSAGE_MANAGE))
            event.getMessage().delete().queue();
    }

}
