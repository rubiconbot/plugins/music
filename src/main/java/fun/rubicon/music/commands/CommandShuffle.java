/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;

public class CommandShuffle extends SameChannelCommand{

    public CommandShuffle() {
        super(new String[] {"shuffle", "mixup", "mix"}, "", "Shuffles the queue up");
    }

    @Override
    Result run(CommandEvent event, String[] args) {
        if(getGuildMusicPlayer(event.getGuild()).getQueue().isEmpty())
            return send(error("Empty queue", "Your queue is emtpy"));
        getGuildMusicPlayer(event.getGuild()).shuffle();
        return send(success("Shuffled up!", "Your queue got shuffled up"));
    }
}
