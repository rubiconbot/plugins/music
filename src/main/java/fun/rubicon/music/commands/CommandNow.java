/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.music.entities.TrackDataHolder;
import fun.rubicon.music.util.EmbedUtil;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;

public class CommandNow extends SameChannelCommand{

    public CommandNow() {
        super(new String[] {"now", "currentrack"}, "", "Displays information about the current track");
    }

    @Override
    Result run(CommandEvent event, String[] args) {
        GuildMusicPlayer guildMusicPlayer = getGuildMusicPlayer(event.getGuild());
        if(!guildMusicPlayer.isPlaying())
            return send(error("Not playing!", "Rubicon does not play any song at the moment."));
        TrackDataHolder t = new TrackDataHolder(guildMusicPlayer.getPlayingTrack());
        return send(EmbedUtil.formatEmbed(t, event.getAuthor()));
    }
}
