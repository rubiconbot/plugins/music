/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music;


import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.http.HttpAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.soundcloud.SoundCloudAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.twitch.TwitchStreamAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager;
import fun.rubicon.music.commands.*;
import fun.rubicon.music.core.GuildMusicPlayerManager;
import fun.rubicon.music.listener.ChooseListener;
import fun.rubicon.music.listener.QueueMessageListener;
import fun.rubicon.plugin.Plugin;
import lavalink.client.io.Lavalink;
import lombok.Getter;

public class MusicPlugin extends Plugin {

    @Getter
    private static MusicPlugin instance;
    @Getter
    private Lavalink lavaLink;
    @Getter
    private GuildMusicPlayerManager guildMusicPlayerManager;
    @Getter
    private AudioPlayerManager audioPlayerManager;

    @Override
    public void init() {
        instance = this;
        lavaLink = super.getRubicon().getLavalink();
        guildMusicPlayerManager = new GuildMusicPlayerManager();
        initAudioPlayerManager();
        registerCommand(new CommandJoin());
        registerCommand(new CommandLeave());
        registerCommand(new CommandPause());
        registerCommand(new CommandPlay());
        registerCommand(new CommandResume());
        registerCommand(new CommandShuffle());
        registerCommand(new CommandSkip());
        registerCommand(new CommandVolume());
        registerCommand(new CommandClearQueue());
        registerCommand(new CommandNow());
        registerCommand(new CommandQueue());
        registerCommand(new CommandRepeat());
        registerCommand(new CommandQueueRepeat());
        registerCommand(new CommandStats());
        registerListener(new ChooseListener());
        registerListener(new QueueMessageListener());
    }

    private void initAudioPlayerManager() {
        audioPlayerManager = new DefaultAudioPlayerManager();
        audioPlayerManager.registerSourceManager(new YoutubeAudioSourceManager());
        audioPlayerManager.registerSourceManager(new SoundCloudAudioSourceManager());
        audioPlayerManager.registerSourceManager(new HttpAudioSourceManager());
        audioPlayerManager.registerSourceManager(new TwitchStreamAudioSourceManager());
    }


}
