/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.util;

import fun.rubicon.music.commands.CommandPlay;
import fun.rubicon.music.entities.TrackDataHolder;
import fun.rubicon.plugin.util.Colors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;

import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

public class EmbedUtil extends fun.rubicon.plugin.util.EmbedUtil {

    public static EmbedBuilder formatEmbed(TrackDataHolder trackData, User user, int queueSize) {
        return build(trackData, user, String.format("Queued - No. %s", String.valueOf(queueSize + 1)));
    }

    public static EmbedBuilder formatEmbed(TrackDataHolder trackData, User user) {
        return build(trackData, user, "Current track");
    }

    private static EmbedBuilder build(TrackDataHolder trackData, User user, String title){
        return new EmbedBuilder()
                .setAuthor(String.format("%s", title), trackData.url, null)
                .addField("Title", trackData.name, true)
                .addField("Author", trackData.author, true)
                .addField("Duration", trackData.isStream ? "Stream" : CommandPlay.getTimestamp(trackData.duration), false)
                .setFooter(String.format("Requested by %s ", user.getName()), user.getAvatarUrl())
                .setColor(colors[ThreadLocalRandom.current().nextInt(colors.length)]);
    }

    private static Color[] colors = {
            Colors.RED,
            Colors.BLUE,
            Colors.GREEN
    };
}
