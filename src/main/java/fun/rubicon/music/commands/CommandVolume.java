/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;

public class CommandVolume extends SameChannelCommand{

    public CommandVolume() {
        super(new String[] {"volume", "setvolume"}, "<volume>", "Set the volume");
    }

    @Override
    Result run(CommandEvent event, String[] args) {
        int volume;
        try {
            volume = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            return send(error("Unknown number!", "You have to specify a valid number."));
        }
        if(volume > 200)
            return send(error("To high volume!", "Volume cannot be higher than 200"));
        getGuildMusicPlayer(event.getGuild()).setVolume(volume);
        return send(success("Volume set!", String.format("Successfully set volume to `%d`", volume)));
    }
}
