/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.core;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import fun.rubicon.music.MusicPlugin;
import lavalink.client.io.Link;
import lavalink.client.player.IPlayer;
import lavalink.client.player.LavaplayerPlayerWrapper;
import lombok.Getter;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.managers.AudioManager;

public class GuildMusicPlayer extends MusicPlayer {

    private final Guild guild;
    @Getter
    private final IPlayer player;
    @Getter
    private final AudioManager audioManager;
    @Getter
    private final AudioPlayerManager audioPlayerManager;

    GuildMusicPlayer(Guild guild){
        this.guild = guild;
        this.audioManager = guild.getAudioManager();
        this.audioPlayerManager = MusicPlugin.getInstance().getAudioPlayerManager();
        this.player = getPlayer(guild.getId());
        initMusicPlayer(player);
    }

    private IPlayer getPlayer(String guildId){
        IPlayer player = getLink(guildId).getPlayer();
        return player == null ? new LavaplayerPlayerWrapper(audioPlayerManager.createPlayer()) : player;
    }

    public void connect(VoiceChannel voiceChannel) {
        getLink(voiceChannel.getGuild()).connect(voiceChannel);
        audioManager.setSelfDeafened(true);
        setVolume(DEFAULT_VOLUME);
    }

    public Link getLink(Guild guild){
        return MusicPlugin.getInstance().getLavaLink().getLink(guild);
    }

    private Link getLink(String guildId){
        return MusicPlugin.getInstance().getLavaLink().getLink(guildId);
    }



    @Override
    public void closeAudioConnection() {
        getLink(guild).disconnect();
    }

    @Override
    protected void savePlayer() {

    }
}
