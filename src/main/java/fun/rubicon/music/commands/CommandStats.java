/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import fun.rubicon.music.MusicPlugin;
import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import lavalink.client.io.Link;
import net.dv8tion.jda.core.EmbedBuilder;

import java.util.Objects;

public class CommandStats extends Command {

    public CommandStats() {
        super(new String[] {"musicstats"}, CommandCategory.MUSIC, "", "Sends you stats about the current music player");
    }

    @Override
    public Result execute(CommandEvent commandEvent, String[] strings) {
        GuildMusicPlayer guildMusicPlayer = MusicPlugin.getInstance().getGuildMusicPlayerManager().getAndCreatePlayer(commandEvent.getGuild());
        Link link = guildMusicPlayer.getLink(commandEvent.getGuild());
        new EmbedBuilder()
                .setTitle("Music statistics")
                .addField("Current node ", Objects.requireNonNull(link.getNode()).getName(), true)
                .addField("Node CPU cores", String.valueOf(Objects.requireNonNull(link.getNode().getStats()).getCpuCores()), true)
                .addField("Current songs in queue",(guildMusicPlayer.isPlaying() ? String.valueOf((guildMusicPlayer.getQueue().size() + 1)) : "Not playing"), false)
                .addField("Current track", (guildMusicPlayer.isPlaying() ? guildMusicPlayer.getPlayingTrack().getIdentifier() : "Not playing"), false)
                .addField("Current player instances", String.valueOf((MusicPlugin.getInstance().getGuildMusicPlayerManager().getPlayerStorage().size() + 1)), false);
        return null;
    }
}
