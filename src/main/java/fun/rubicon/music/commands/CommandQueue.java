/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.music.entities.QueueMessage;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import fun.rubicon.plugin.util.Colors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static fun.rubicon.music.commands.CommandPlay.getTimestamp;

public class CommandQueue extends SameChannelCommand {

    public CommandQueue() {
        super(new String[] {"queue"}, "", "Displays the current queue");
    }

    @Override
    Result run(CommandEvent event, String[] args) {
        GuildMusicPlayer guildMusicPlayer = getGuildMusicPlayer(event.getGuild());
        if (!guildMusicPlayer.isPlaying())
            return send(error("Not playing!", "Rubicon does not play any song at the moment."));
        try {
            int sideNumb = args.length > 0 ? Integer.parseInt(args[0]) : 1;
            List<String> tracks = new ArrayList<>();
            List<String> tracksSubList;
            tracks.add(buildQueueEntry(guildMusicPlayer.getPlayingTrack(), true, guildMusicPlayer.isRepeating()));
            guildMusicPlayer.getTrackList().forEach(track -> tracks.add(buildQueueEntry(track, false, false)));
            int sideNumbAll = tracks.size() >= 20 ? tracks.size() / 20 : 1;
            if (sideNumb > sideNumbAll) {
                return send(error("Page not found!", "This page does not exist"));
            }
            if (tracks.size() > 20)
                tracksSubList = tracks.subList((sideNumb - 1) * 20, (sideNumb - 1) * 20 + 20);
            else
                tracksSubList = tracks;

            String formattedQueue = tracksSubList.stream().collect(Collectors.joining("\n"));
            Message msg = sendMessageBlocking(event.getTextChannel(),
                    new EmbedBuilder().setDescription("**CURRENT QUEUE" + (guildMusicPlayer.isQueueRepeating() ? " (Repeating)" : "") + ":**\n" +
                            "*[" + (guildMusicPlayer.getTrackList().size() + 1) + " Tracks | Page " + sideNumb + " / " + sideNumbAll + "]* \n" +
                            formattedQueue
                    ).setColor(Colors.GREEN).build());
            if (tracks.size() > 20) {
                msg.addReaction("➡").queue();
                new QueueMessage(sideNumbAll, msg, tracks, event.getAuthor());
            }
        } catch (NumberFormatException e) {
            return send(error("Invalid number!", "You have to specify a valid number"));
        }
        return null;
    }
        private String buildQueueEntry(AudioTrack track, boolean current, boolean queueRepeat) {
            if (track == null)
                return "";
            AudioTrackInfo info = track.getInfo();
            return "`[ " + getTimestamp(info.length) + " ] " + info.title + "`" + (current ? " (Current song)" : "") + (queueRepeat ? " (Repeating)" : "");
        }

}
