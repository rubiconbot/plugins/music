/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;

public class CommandRepeat extends SameChannelCommand {

    public CommandRepeat() {
        super(new String[] {"repeat", "loop"}, "", "Plays the current song on repeat");
    }

    @Override
    Result run(CommandEvent event, String[] args) {
        GuildMusicPlayer guildMusicPlayer = getGuildMusicPlayer(event.getGuild());
        if(!guildMusicPlayer.isPlaying())
            return send(error("Not playing!", "Rubicon does not play any song at the moment."));
        if(guildMusicPlayer.isQueueRepeating())
            return send(error("Already queue repeating!", "Rubicon does already loop the queue"));
        if(!guildMusicPlayer.isRepeating()){
            guildMusicPlayer.setRepeating(true);
            return send(success("Repeat enabled", "Rubicon does now repeat the current song"));
        } else {
            guildMusicPlayer.setRepeating(false);
            guildMusicPlayer.getQueue().clear();
            return send(success("Repeat disabled", "Rubicon does no longer repeat the current song"));
        }
    }
}
