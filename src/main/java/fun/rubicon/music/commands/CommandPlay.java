/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.commands;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fun.rubicon.music.core.GuildMusicPlayer;
import fun.rubicon.music.entities.MusicSearchResult;
import fun.rubicon.music.entities.TrackDataHolder;
import fun.rubicon.music.util.EmbedUtil;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import lombok.extern.log4j.Log4j;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Message;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

@Log4j
public class CommandPlay extends SameChannelCommand {

    public CommandPlay() {
        super(new String[] {"play"}, "<search/url>", "Play a song with rubicon");
    }

    @Override
    public Result execute(CommandEvent event, String[] args) {
        if(args.length == 0)
            sendHelp(event);
        if(event.getGuild().getSelfMember().getVoiceState().inVoiceChannel()) {
            return super.execute(event, args);
        } else {
            if(!event.getGuild().getSelfMember().hasPermission(event.getMember().getVoiceState().getChannel(), Permission.VOICE_CONNECT))
                return send(error("No permissions!", "Rubicon is not permitted to join your channel."));
            getGuildMusicPlayer(event.getGuild()).connect(event.getMember().getVoiceState().getChannel());
            return run(event, args);
        }
    }

    @Override
    Result run(CommandEvent event, String[] args) {
        GuildMusicPlayer guildMusicPlayer = getGuildMusicPlayer(event.getGuild());
        String keyword = event.getArgsAsString();
        boolean isUrl = true;
        if(!keyword.startsWith("http://") && !keyword.startsWith("https://")){
            keyword = "ytsearch: " + keyword;
            isUrl = false;
        }
        final boolean isURL = isUrl;
        Message message = sendMessageBlocking(event.getTextChannel(), info("Searching ...", "Searching videos").build());
        guildMusicPlayer.getAudioPlayerManager().loadItemOrdered(guildMusicPlayer.getPlayer().toString(), keyword, new AudioLoadResultHandler() {

            @Override
            public void trackLoaded(AudioTrack audioTrack) {
                TrackDataHolder trackData = new TrackDataHolder(audioTrack);
                guildMusicPlayer.queueTrack(audioTrack);
                message.editMessage(EmbedUtil.formatEmbed(trackData, event.getAuthor(), guildMusicPlayer.getQueueSize()).build()).queue();
            }

            @Override
            public void playlistLoaded(AudioPlaylist audioPlaylist) {
                List<AudioTrack> playlistTracks = audioPlaylist.getTracks();
                playlistTracks = playlistTracks.stream().limit(guildMusicPlayer.QUEUE_MAXIMUM - guildMusicPlayer.getQueueSize()).collect(Collectors.toList());
                if(isURL) {
                    playlistTracks.forEach(guildMusicPlayer::queueTrack);
                    message.editMessage(EmbedUtil.formatEmbed(new TrackDataHolder(guildMusicPlayer.getPlayingTrack()), event.getAuthor())
                            .setDescription(String.format("Loaded `%s` tracks from playlist `%s`", playlistTracks.size(), audioPlaylist.getName())).build()).queue();
                    return;
                }
                MusicSearchResult musicSearchResult = new MusicSearchResult(event.getMember(), guildMusicPlayer);
                audioPlaylist.getTracks().stream().limit(5).collect(Collectors.toList()).forEach(track -> {
                    try {
                        musicSearchResult.addTrack(track);
                    } catch (Exception e) {
                        log.error(e);
                    }
                });
                message.editMessage(musicSearchResult.generateEmbed().build()).queue();
                musicSearchResult.setChooseMessage(message);
                MusicSearchResult.musicChoose.add(musicSearchResult);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (MusicSearchResult.musicChoose.contains(musicSearchResult)) {
                            musicSearchResult.getChooseMessage().delete().queue();
                            MusicSearchResult.musicChoose.remove(musicSearchResult);
                        }
                    }
                }, 15000);

            }

            @Override
            public void noMatches() {
                message.editMessage(error("No results found!", "No results found.").build()).queue();
            }

            @Override
            public void loadFailed(FriendlyException e) {
                message.editMessage(error("Unknown error occurred!", String.format("An unknown error occurred: %s!", e.getMessage())).build()).queue();

            }
        });
        return null;
    }

    public static String getTimestamp(long milliseconds) {
            int seconds = (int) (milliseconds / 1000) % 60;
            int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
            int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

            if (hours > 0)
                return String.format("%02d:%02d:%02d", hours, minutes, seconds);
            else if (minutes > 0)
                return String.format("%02d:%02d", minutes, seconds);
            else
                return String.format("00:%02d", seconds);
    }
}
