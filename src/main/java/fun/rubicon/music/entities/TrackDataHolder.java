/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.entities;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

public class TrackDataHolder {

    public String name;
    public String author;
    public String url;
    public boolean isStream;
    public long duration;

    public TrackDataHolder(AudioTrack track) {
        this.name = track.getInfo().title;
        this.author = track.getInfo().author;
        this.url = track.getInfo().uri;
        this.isStream = track.getInfo().isStream;
        this.duration = track.getDuration();
    }
}
