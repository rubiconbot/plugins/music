/*
 * RubiconBot - An open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.music.core;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import lavalink.client.player.IPlayer;
import lavalink.client.player.event.PlayerEventListenerAdapter;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.core.audio.AudioSendHandler;

import java.util.*;

public abstract class MusicPlayer extends PlayerEventListenerAdapter implements AudioSendHandler {

    private Queue<AudioTrack> trackQueue;
    private IPlayer player;
    @Getter @Setter
    private boolean repeating;
    @Getter @Setter
    private boolean queueRepeating;
    private boolean stayInChannel;

    final int DEFAULT_VOLUME = 10;
    public final int QUEUE_MAXIMUM = 50;

    MusicPlayer() {
        trackQueue = new LinkedList<>();
        repeating = false;
        queueRepeating = false;
        //When this variable is true you will die automatically
        stayInChannel = false;
    }

    void initMusicPlayer(IPlayer player) {
        this.player = player;
        player.addListener(this);
    }

    public void play(AudioTrack track) {
        if (track == null) {
            System.out.println("null");
            if (stayInChannel)
                return;
            else
                closeAudioConnection();
            return;
        }
        if (player.isPaused())
            player.setPaused(false);
        player.playTrack(track);
    }

    public void stop() {
        player.stopTrack();
        clearQueue();
    }

    public void pause() {
        player.setPaused(true);
    }

    public void resume() {
        player.setPaused(false);
    }

    public void seek(long time) {
        player.seekTo(time);
    }

    public void shuffle() {
        Collections.shuffle((List<?>) trackQueue);
    }

    public AudioTrack getPlayingTrack() {
        return player.getPlayingTrack();
    }

    public void queueTrack(AudioTrack audioTrack) {
        trackQueue.add(audioTrack);
        if (player.getPlayingTrack() == null)
            play(pollTrack());
    }

    public AudioTrack pollTrack() {
        if (trackQueue.isEmpty())
            return null;
        AudioTrack track = trackQueue.poll();
        savePlayer();
        return track;
    }

    public boolean isPlaying() {
        return getPlayingTrack() != null;
    }

    public void setVolume(int volume){
        player.setVolume(volume);
    }

    private void clearQueue() {
        trackQueue.clear();
        savePlayer();
    }

    public List<AudioTrack> getTrackList() {
        return new ArrayList<>(trackQueue);
    }

    public int getQueueSize() {
        return trackQueue.size();
    }

    public Queue<AudioTrack> getQueue() {
        return trackQueue;
    }

    private void handleTrackStop() {
        queueTrack(pollTrack());
    }

    @Override
    public void onTrackEnd(IPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
        if (endReason.equals(AudioTrackEndReason.FINISHED)) {
            AudioTrack nextTrack = pollTrack();
            if (repeating)
                nextTrack = track;
            if (nextTrack == null) {
                closeAudioConnection();
                return;
            }
            play(nextTrack);
            if(queueRepeating)
                queueTrack(track);
            return;
        }
        if (!endReason.equals(AudioTrackEndReason.LOAD_FAILED)) {
            handleTrackStop();
        } else {
            handleTrackStop();
        }
    }

    protected abstract void closeAudioConnection();

    protected abstract void savePlayer();

    @Override
    public void onTrackException(IPlayer player, AudioTrack track, Exception exception) {
        handleTrackStop();
    }

    @Override
    public boolean canProvide() {
        return false;
    }

    @Override
    public byte[] provide20MsAudio() {
        return new byte[0];
    }

    @Override
    public boolean isOpus() {
        return false;
    }
}
